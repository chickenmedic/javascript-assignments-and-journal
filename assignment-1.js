/*
Well done here, Sam.  Your application works great.
You've got the html and JS comments I needed to see.
The only thing I missed was the name of the province array
It should have been name provArray.  Everything else
appears to have been named correctly
19/20
*/
        function loadProvinces(){

            var provincesArray  = ["Alberta", "British Colombia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan"];
            var selectBox;
            selectBox = document.getElementById('cboProv');

                for(i=0; i<provincesArray.length; i++){
                    var opt = document.createElement('option');
                    opt.value = provincesArray[i];
                    opt.innerHTML = provincesArray[i];
                    selectBox.appendChild(opt);

                }
        }
        // This is a javascript commnet, letting you know that this part of the code is for the validation.
        //isn't that just fantastic
        function validateForm(){
        var province = document.getElementById('cboProv').value;
        var name = document.getElementById('txtName').value;
        var email = document.getElementById('txtEmail').value;

        if(province==""){
            alert('please select a Province');
            document.getElementById('cboProv').focus();
            return false;
        }
        if(name==""){
            alert('please enter a name');
            document.getElementById('txtName').focus();
            return false;
        }
        if(email==""){
            alert('please enter an email');
            document.getElementById('txtEmail').focus();
            return false;
        }
        alert('congrats, you entered the form correctly');
    }
